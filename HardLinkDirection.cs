﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGS.Plugin.Libraries
{
    public enum HardLinkDirection
    {
        GlobalCacheToLocal,
        LocalToGlobalCache
    }
}
