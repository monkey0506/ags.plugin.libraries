﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;

namespace AGS.Plugin.Libraries
{
    /// <summary>
    /// Borrowed from /ags/Editor/AGS.Editor/Utils/Utilities.cs
    /// These methods are unlikely to change frequently and are not general enough
    /// to warrant placing them in AGS.Types.Utilities.
    /// </summary>
    internal static class Utilities
    {
        private const int ERROR_NO_MORE_FILES = 18;

        public static bool IsMonoRunning()
        {
            return Type.GetType("Mono.Runtime") != null;
        }

        /// <summary>
        /// Wraps Directory.GetFiles in a handler to deal with an exception
        /// erroneously being thrown on Linux network shares if no files match.
        /// </summary>
        public static string[] GetDirectoryFileList(string directory, string fileMask)
        {
            return GetDirectoryFileList(directory, fileMask, SearchOption.TopDirectoryOnly);
        }

        /// <summary>
        /// Wraps Directory.GetFiles in a handler to deal with an exception
        /// erroneously being thrown on Linux network shares if no files match.
        /// </summary>
        public static string[] GetDirectoryFileList(string directory, string fileMask, SearchOption searchOption)
        {
            try
            {
                return Directory.GetFiles(directory, fileMask, searchOption);
            }
            catch (IOException)
            {
                if (Marshal.GetLastWin32Error() == ERROR_NO_MORE_FILES)
                {
                    // On a network share the Framework can throw this if
                    // there are no matching files (reported by RickJ)...
                    // Seems to be a Win32 FindFirstFile bug in certain
                    // circumstances.
                    return new string[0];
                }
                throw;
            }
        }

        /// <summary>
        /// Sets security permissions for a specific file.
        /// </summary>
        public static void SetFileAccess(string fileName, SecurityIdentifier sid, FileSystemRights rights, AccessControlType type)
        {
            try
            {
                FileSecurity fsec = File.GetAccessControl(fileName);
                fsec.AddAccessRule(new FileSystemAccessRule(sid, rights, type));
                File.SetAccessControl(fileName, fsec);
            }
            catch (UnauthorizedAccessException)
            {
            }
        }

        public static void SetDirectoryFilesAccess(string directory)
        {
            SetDirectoryFilesAccess(directory, new SecurityIdentifier(WellKnownSidType.WorldSid, null));
        }

        public static void SetDirectoryFilesAccess(string directory, SecurityIdentifier sid)
        {
            SetDirectoryFilesAccess(directory, sid, FileSystemRights.Modify);
        }

        public static void SetDirectoryFilesAccess(string directory, SecurityIdentifier sid, FileSystemRights rights)
        {
            SetDirectoryFilesAccess(directory, sid, rights, AccessControlType.Allow);
        }

        /// <summary>
        /// Sets security permissions for all files in a directory.
        /// </summary>
        public static void SetDirectoryFilesAccess(string directory, SecurityIdentifier sid, FileSystemRights rights, AccessControlType type)
        {
            try
            {
                // first we will attempt to take ownership of the file
                // this ensures (if successful) that all users can change security
                // permissions for the file without admin access
                ProcessStartInfo si = new ProcessStartInfo("cmd.exe");
                si.RedirectStandardInput = false;
                si.RedirectStandardOutput = false;
                si.RedirectStandardError = false;
                si.UseShellExecute = false;
                si.Arguments = string.Format("/c takeown /f \"{0}\" /r /d y", directory);
                si.CreateNoWindow = true;
                si.WindowStyle = ProcessWindowStyle.Hidden;
                if (IsMonoRunning())
                {
                    si.FileName = "chown";
                    si.Arguments = string.Format("-R $USER:$USER \"{0}\"", directory);
                }
                Process process = Process.Start(si);
                bool result = (process != null);
                if (result)
                {
                    process.EnableRaisingEvents = true;
                    process.WaitForExit();
                    if (process.ExitCode != 0) return;
                    process.Close();
                    // after successfully gaining ownership, parse each file in the
                    // directory (recursively) and update its access rights
                    foreach (string filename in GetDirectoryFileList(directory, "*", SearchOption.AllDirectories))
                    {
                        SetFileAccess(filename, sid, rights, type);
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
        }

        public static bool IsWindowsXPOrHigher()
        {
            OperatingSystem os = Environment.OSVersion;
            // Windows XP reports platform as Win32NT and version number >= 5.1
            return ((os.Platform == PlatformID.Win32NT) && ((os.Version.Major > 5) || ((os.Version.Major == 5) && (os.Version.Minor == 1))));
        }

        public static bool IsWindowsVistaOrHigher()
        {
            OperatingSystem os = Environment.OSVersion;
            // Windows Vista reports platform as Win32NT and version number >= 6
            return ((os.Platform == PlatformID.Win32NT) && (os.Version.Major >= 6));
        }

        public static bool CreateHardLink(string destFileName, string sourceFileName)
        {
            return CreateHardLink(destFileName, sourceFileName, false);
        }

        public static bool CreateHardLink(string destFileName, string sourceFileName, bool overwrite)
        {
            if (File.Exists(destFileName))
            {
                if (overwrite) File.Delete(destFileName);
                else return false;
            }
            char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
            if (Path.GetFileName(destFileName).IndexOfAny(invalidFileNameChars) != -1)
            {
                throw new ArgumentException("Cannot create hard link! Invalid destination file name. (" + destFileName + ")");
            }
            if (Path.GetFileName(sourceFileName).IndexOfAny(invalidFileNameChars) != -1)
            {
                throw new ArgumentException("Cannot create hard link! Invalid source file name. (" + sourceFileName + ")");
            }
            if (!File.Exists(sourceFileName))
            {
                throw new FileNotFoundException("Cannot create hard link! Source file does not exist. (" + sourceFileName + ")");
            }
            ProcessStartInfo si = new ProcessStartInfo("cmd.exe");
            si.RedirectStandardInput = false;
            si.RedirectStandardOutput = false;
            si.RedirectStandardError = false;
            si.UseShellExecute = false;
            si.Arguments = string.Format("/c mklink /h \"{0}\" \"{1}\"", destFileName, sourceFileName);
            si.CreateNoWindow = true;
            si.WindowStyle = ProcessWindowStyle.Hidden;
            if ((!IsWindowsVistaOrHigher()) && (IsWindowsXPOrHigher())) // running Windows XP
            {
                si.Arguments = string.Format("/c fsutil hardlink create \"{0}\" \"{1}\"", destFileName, sourceFileName);
            }
            if (IsMonoRunning())
            {
                si.FileName = "ln";
                si.Arguments = string.Format("\"{0}\" \"{1}\"", sourceFileName, destFileName);
            }
            Process process = Process.Start(si);
            bool result = (process != null);
            if (result)
            {
                process.EnableRaisingEvents = true;
                process.WaitForExit();
                if (process.ExitCode == 1)
                {
                    System.Windows.Forms.MessageBox.Show("Error! Make sure that the source and destination paths are on the same drive.");
                }
                if (process.ExitCode != 0) return false;
                process.Close();
            }
            return result;
        }
    }
}
