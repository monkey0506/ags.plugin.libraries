﻿using AGS.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGS.Plugin.Libraries
{
    [RequiredAGSVersion("3.4.0.2")]
    public class LibrariesPlugin : IAGSEditorPlugin, IDisposable
    {
        public const string FULL_NAME = "AGS.Plugin.Libraries";
        private static IAGSEditor _editor;
        private static IRePopulatableComponent _scriptsComponent;
        private static LibrariesPane _librariesPane;

        public LibrariesPlugin(IAGSEditor editor)
        {
            editor.AddComponent(new LibrariesComponent(editor));
            _editor = editor;
        }

        public void Dispose()
        {
        }

        public static IAGSEditor EditorInstance
        {
            get
            {
                return _editor;
            }
        }

        public static IRePopulatableComponent ScriptsComponent
        {
            get
            {
                if (_scriptsComponent == null)
                {
                    foreach (IEditorComponent component in _editor.Components)
                    {
                        IRePopulatableComponent recomponent = (component as IRePopulatableComponent);
                        if ((component.ComponentID == "Scripts") && (recomponent != null))
                        {
                            _scriptsComponent = recomponent;
                            break;
                        }
                    }
                }
                return _scriptsComponent;
            }
        }

        public static LibrariesPane LibrariesPane
        {
            get
            {
                if (_librariesPane == null)
                {
                    _librariesPane = new LibrariesPane();
                }
                return _librariesPane;
            }
        }
    }
}
