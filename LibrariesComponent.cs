﻿using AGS.Types;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

namespace AGS.Plugin.Libraries
{
    public class LibrariesComponent : IEditorComponent
    {
        private const string COMPONENT_ID = "LibrariesComponent";
        private const string CONTROL_ID_ROOT_NODE = "LibrariesPluginRoot";
        private const string CONTROL_ID_CONTEXT_MENU_ADD = "LibrariesPluginAdd";
        private const string CONTROL_ID_CONTEXT_MENU_EXCLUDE = "LibrariesPluginExclude";
        private const string CONTROL_ID_CONTEXT_MENU_REMOVE = "LibrariesPluginRemove";
        private const string CONTROL_ID_CONTEXT_MENU_MAKE_UNIQUE = "LibrariesPluginMakeUnique";
        private const string CONTROL_ID_CONTEXT_MENU_SET_CACHE_DIRECTORY = "LibrariesPluginSetCacheDirectory";
        private const string ICON_KEY = "LibrariesPluginIcon";
        private readonly IAGSEditor _editor;
        private ScriptAndHeader _lastScript;
        private Dictionary<string, MenuCommand> _menuCommands;
        private List<bool> _libraryIncluded;
        private ContentDocument _librariesPaneDocument;

        public LibrariesComponent(IAGSEditor editor)
        {
            _editor = editor;
            _editor.GUIController.RegisterIcon(ICON_KEY, GetIcon(LibrariesPlugin.FULL_NAME + ".ico"));
            _editor.GUIController.ProjectTree.AddTreeRoot(this, CONTROL_ID_ROOT_NODE, "Libraries", ICON_KEY);
            _editor.GUIController.ProjectTree.BeforeShowContextMenu += new BeforeShowContextMenuHandler(BeforeShowContextMenu);
            _menuCommands = new Dictionary<string, MenuCommand>();
            MenuCommand command = _editor.GUIController.CreateMenuCommand(this, CONTROL_ID_CONTEXT_MENU_ADD, "Add to libraries");
            _menuCommands.Add(CONTROL_ID_CONTEXT_MENU_ADD, command);
            command = _editor.GUIController.CreateMenuCommand(this, CONTROL_ID_CONTEXT_MENU_EXCLUDE, "Exclude this library");
            _menuCommands.Add(CONTROL_ID_CONTEXT_MENU_EXCLUDE, command);
            command = _editor.GUIController.CreateMenuCommand(this, CONTROL_ID_CONTEXT_MENU_REMOVE, "Remove from libraries");
            _menuCommands.Add(CONTROL_ID_CONTEXT_MENU_REMOVE, command);
            command = _editor.GUIController.CreateMenuCommand(this, CONTROL_ID_CONTEXT_MENU_MAKE_UNIQUE, "Make unique copy");
            _menuCommands.Add(CONTROL_ID_CONTEXT_MENU_MAKE_UNIQUE, command);
            command = _editor.GUIController.CreateMenuCommand(this, CONTROL_ID_CONTEXT_MENU_SET_CACHE_DIRECTORY, "Set cache directory");
            _menuCommands.Add(CONTROL_ID_CONTEXT_MENU_SET_CACHE_DIRECTORY, command);
            _libraryIncluded = new List<bool>(Libraries.AllLibraries.Count);
            _librariesPaneDocument = new ContentDocument(LibrariesPlugin.LibrariesPane, "Libraries", this, ICON_KEY);
        }

        void IEditorComponent.BeforeSaveGame()
        {
            Libraries.WriteMetaDataFile();
        }

        void IEditorComponent.CommandClick(string controlID)
        {
            if (controlID == CONTROL_ID_ROOT_NODE)
            {
                _editor.GUIController.AddOrShowPane(_librariesPaneDocument);
            }
            else if (controlID == CONTROL_ID_CONTEXT_MENU_ADD)
            {
                if (!Libraries.IsLibrary(_lastScript))
                {
                    Libraries.AddLibrary(new Library(_lastScript));
                }
            }
            else if (controlID == CONTROL_ID_CONTEXT_MENU_EXCLUDE)
            {
                Library library = Libraries.FindLibraryByScriptAndHeader(_lastScript);
                if (library == null) return;
                library.Included = false;
            }
            else if (controlID == CONTROL_ID_CONTEXT_MENU_REMOVE)
            {
                Library library = Libraries.FindLibraryByScriptAndHeader(_lastScript);
                if (library == null) return;
                Libraries.RemoveLibrary(library);
            }
            else if (controlID == CONTROL_ID_CONTEXT_MENU_MAKE_UNIQUE)
            {
                Library library = Libraries.FindLibraryByScriptAndHeader(_lastScript);
                if (library == null) return;
                library.MakeUniqueCopy();
            }
            else if (controlID == CONTROL_ID_CONTEXT_MENU_SET_CACHE_DIRECTORY)
            {
                Libraries.SelectCacheDirectory();
            }
        }

        string IEditorComponent.ComponentID
        {
            get
            {
                return COMPONENT_ID;
            }
        }

        void IEditorComponent.EditorShutdown()
        {
        }

        void IEditorComponent.FromXml(XmlNode node)
        {
        }

        void IEditorComponent.GameSettingsChanged()
        {
        }

        IList<MenuCommand> IEditorComponent.GetContextMenu(string controlID)
        {
            List<MenuCommand> commands = new List<MenuCommand>();
            commands.Add(_menuCommands[CONTROL_ID_CONTEXT_MENU_SET_CACHE_DIRECTORY]);
            return commands;
        }

        void IEditorComponent.PropertyChanged(string propertyName, object oldValue)
        {
        }

        void IEditorComponent.RefreshDataFromGame()
        {
            _editor.GUIController.RemovePaneIfExists(_librariesPaneDocument);
            Libraries.ReadFromMetaDataFile();
        }

        void IEditorComponent.ToXml(XmlTextWriter writer)
        {
        }

        private Icon GetIcon(string fileName)
        {
            return new Icon(base.GetType(), fileName);
        }

        private void BeforeShowContextMenu(BeforeShowContextMenuEventArgs e)
        {
            if ((e.Component == LibrariesPlugin.ScriptsComponent) && (e.SelectedNodeID != "ScriptsCmd"))
            {
                string prefix = "ScriptAndHeader";
                string scriptName = e.SelectedNodeID.Substring(prefix.Length);
                if ((!scriptName.EndsWith(Library.HEADER_FILE_EXT)) && (!scriptName.EndsWith(Library.SCRIPT_FILE_EXT)))
                {
                    scriptName += Library.SCRIPT_FILE_EXT;
                    int idx;
                    _lastScript = _editor.CurrentGame.ScriptsAndHeaders.GetScriptAndHeaderByFilename(scriptName, out idx);
                    if (_lastScript == null) return;
                    e.MenuCommands.Commands.Add(MenuCommand.Separator);
                    if (!Libraries.IsLibrary(_lastScript))
                    {
                        e.MenuCommands.Commands.Add(_menuCommands[CONTROL_ID_CONTEXT_MENU_ADD]);
                    }
                    else
                    {
                        e.MenuCommands.Commands.Add(_menuCommands[CONTROL_ID_CONTEXT_MENU_EXCLUDE]);
                        e.MenuCommands.Commands.Add(_menuCommands[CONTROL_ID_CONTEXT_MENU_MAKE_UNIQUE]);
                        e.MenuCommands.Commands.Add(_menuCommands[CONTROL_ID_CONTEXT_MENU_REMOVE]);
                    }
                }
            }
        }
    }
}
