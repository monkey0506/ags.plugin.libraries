﻿using AGS.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace AGS.Plugin.Libraries
{
    internal static class Libraries
    {
        private const string META_DATA_FILE_EXT = ".lbm";
        private static readonly string _metadataDirectory;
        private static string _cacheDirectory;
        private static List<Library> _libraries;

        static Libraries()
        {
            _metadataDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), LibrariesPlugin.FULL_NAME);
            _cacheDirectory = _metadataDirectory;
            _libraries = new List<Library>();
            ReadFromMetaDataFile();
        }

        public static void AddLibrary(Library library)
        {
            if (library == null) return;
            string projectDirRoot = Directory.GetDirectoryRoot(GetProjectFilePath(null));
            while (Directory.GetDirectoryRoot(CacheDirectory) != projectDirRoot)
            {
                if (DialogResult.Yes == MessageBox.Show("Sorry! The currently selected cache for library files (" + CacheDirectory +
                    ") must be on the same physical drive as the current project (" + projectDirRoot +
                    "). Would you like to select another location to cache library files now? You must do this to add the script as a library.",
                    "Error: Invalid cache location", MessageBoxButtons.YesNo, MessageBoxIcon.Error))
                {
                    SelectCacheDirectory();
                }
                else return;
            }
            foreach (Library lib in _libraries)
            {
                if (lib.ScriptAndHeader == library.ScriptAndHeader) return;
            }
            _libraries.Add(library);
            LibrariesPlugin.LibrariesPane.AddItem(library);
            Utilities.CreateHardLink(GetFilePath(library.CachedHeaderFileName), GetProjectFilePath(library.Header.FileName), true);
            Utilities.CreateHardLink(GetFilePath(library.CachedScriptFileName), GetProjectFilePath(library.Script.FileName), true);
        }

        public static void RemoveLibrary(Library library)
        {
            if (library == null) return;
            if (DialogResult.No == MessageBox.Show("Are you sure you want to remove this library ('" + library.ToString() + "')?",
                "Remove library?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                return;
            }
            _libraries.Remove(library);
            LibrariesPlugin.LibrariesPane.Items.Remove(library);
            File.Delete(GetFilePath(library.CachedHeaderFileName)); // does not throw
            File.Delete(GetFilePath(library.CachedScriptFileName));
            if (DialogResult.Yes == MessageBox.Show("Remove local script?", "Remove local?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                library.ScriptIncludedInProject = false;
            }
        }

        public static bool IsLibrary(ScriptAndHeader scriptAndHeader)
        {
            return (FindLibraryByScriptAndHeader(scriptAndHeader) != null);
        }

        public static Library FindLibraryByScriptAndHeader(ScriptAndHeader scriptAndHeader)
        {
            foreach (Library library in _libraries)
            {
                if (library.ScriptAndHeader == scriptAndHeader) return library;
            }
            return null;
        }

        public static string GetFilePath(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return CacheDirectory;
            return Path.Combine(CacheDirectory, fileName);
        }

        public static string GetProjectFilePath(string fileName)
        {
            IAGSEditor editor = LibrariesPlugin.EditorInstance;
            if ((editor == null) || (editor.CurrentGame == null)) return fileName;
            if (string.IsNullOrEmpty(fileName)) return editor.CurrentGame.DirectoryPath;
            return Path.Combine(editor.CurrentGame.DirectoryPath, fileName);
        }

        internal static void ReadFromMetaDataFile()
        {
            IAGSEditor editor = LibrariesPlugin.EditorInstance;
            string fileName = MetaDataFilePath;
            if ((editor == null) || (editor.CurrentGame == null) || (!File.Exists(fileName))) return;
            _libraries.Clear();
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            foreach (XmlNode node in doc.SelectSingleNode("Libraries").ChildNodes)
            {
                if (node.Name == "CacheDirectory")
                {
                    if (!string.IsNullOrEmpty(node.InnerText))
                    {
                        _cacheDirectory = node.InnerText;
                        Directory.CreateDirectory(_cacheDirectory);
                    }
                    continue;
                }
                else if (node.Name == "Library")
                {
                    Library library = null;
                    try
                    {
                        library = new Library(node);
                        _libraries.Add(library);
                        int idx;
                        ScriptAndHeader script = editor.CurrentGame.ScriptsAndHeaders.GetScriptAndHeaderByFilename(library.Header.FileName, out idx);
                        if (script == null) continue;
                        bool included = ((library.HasLocalFiles) && (script.Header.UniqueKey == library.UniqueKey));
                        LibrariesPlugin.LibrariesPane.Items.Add(library, included);
                    }
                    catch (FileNotFoundException e)
                    {
                        System.Windows.Forms.MessageBox.Show(e.Message, "Library files missing" + (library == null ? "" : " (" + library.ToString() + ")"),
                            System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
        }

        internal static void WriteMetaDataFile()
        {
            XmlWriterSettings ws = new XmlWriterSettings();
            ws.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(MetaDataFilePath, ws))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Libraries");
                writer.WriteElementString("CacheDirectory", _cacheDirectory);
                foreach (Library library in _libraries)
                {
                    writer.WriteStartElement("Library");
                    writer.WriteElementString("Author", library.Author);
                    writer.WriteElementString("Description", library.Description);
                    writer.WriteElementString("FileName", library.FileName);
                    writer.WriteElementString("Name", library.Name);
                    writer.WriteElementString("UniqueKey", library.UniqueKey.ToString());
                    writer.WriteElementString("Version", library.Version);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public static void SelectCacheDirectory()
        {
            using (FolderBrowserDialog cacheDialog = new FolderBrowserDialog())
            {
                cacheDialog.Description = "Select a folder to store cached library scripts";
                if (cacheDialog.ShowDialog() == DialogResult.OK)
                {
                    foreach (string file in Utilities.GetDirectoryFileList(_cacheDirectory, "*.as?"))
                    {
                        File.Move(file, Path.Combine(cacheDialog.SelectedPath, Path.GetFileName(file)));
                    }
                    _cacheDirectory = cacheDialog.SelectedPath;
                }
            }
        }

        public static string CacheDirectory
        {
            get
            {
                Directory.CreateDirectory(_cacheDirectory); // does not throw
                return _cacheDirectory;
            }
        }

        public static IList<Library> AllLibraries
        {
            get
            {
                return _libraries.AsReadOnly();
            }
        }

        public static string MetaDataFileName
        {
            get
            {
                return LibrariesPlugin.FULL_NAME + META_DATA_FILE_EXT;
            }
        }

        private static string MetaDataFilePath
        {
            get
            {
                return Path.Combine(_metadataDirectory, MetaDataFileName);
            }
        }
    }
}
