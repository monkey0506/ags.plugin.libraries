﻿using AGS.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace AGS.Plugin.Libraries
{
    public partial class LibrariesPane : EditorContentPanel
    {
        public LibrariesPane()
        {
            InitializeComponent();
        }

        public int AddItem(Library library)
        {
            clbLibraries.ItemCheck -= new ItemCheckEventHandler(clbLibraries_ItemCheck);
            int result = clbLibraries.Items.Add(library, true);
            clbLibraries.ItemCheck += new ItemCheckEventHandler(clbLibraries_ItemCheck);
            return result;
        }

        public CheckedListBox.ObjectCollection Items
        {
            get
            {
                return clbLibraries.Items;
            }
        }

        public bool GetItemChecked(Library library)
        {
            if (library == null) return false;
            int index = Items.IndexOf(library);
            return (index == -1 ? false : clbLibraries.GetItemChecked(index));
        }

        public bool GetItemChecked(int index)
        {
            return clbLibraries.GetItemChecked(index);
        }

        public void SetItemChecked(Library library, bool value)
        {
            if (library == null) return;
            int index = Items.IndexOf(library);
            if (index == -1) return;
            clbLibraries.SetItemChecked(index, value);
        }

        public void SetItemChecked(int index, bool value)
        {
            clbLibraries.SetItemChecked(index, value);
        }

        private void clbLibraries_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            bool value = (e.NewValue == CheckState.Checked);
            CheckedListBox clb = (CheckedListBox)sender;
            Library item = (Library)(clb.Items[e.Index]);
            if (value)
            {
                bool force = true;
                string globalHeaderText = File.ReadAllText(Libraries.GetFilePath(item.CachedHeaderFileName));
                string globalScriptText = File.ReadAllText(Libraries.GetFilePath(item.CachedScriptFileName));
                if ((item.Header.Text == globalHeaderText) && (item.Script.Text == globalScriptText))
                {
                    // this is here to prevent the overwrite dialog if files are the same
                    force = false;
                }
                else if (item.HasLocalFiles) // files are not the same, but local files exist
                {
                    force = (DialogResult.Yes == MessageBox.Show("Overwrite local files for Library '" + item.ToString() + "'?", "Overwrite?",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                    if (!force)
                    {
                        // user opted not to overwrite local scripts, library cannot be included
                        clb.ItemCheck -= clbLibraries_ItemCheck; // prevent delete dialog
                        SetItemChecked(item, false);
                        clb.ItemCheck += clbLibraries_ItemCheck;
                    }
                }
                item.ScriptIncludedInProject = true;
                //item.CreateLocalFiles(force);
            }
            else
            {
                if (DialogResult.Yes == MessageBox.Show("Delete local files?", "Delete local?",
                    MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question))
                {
                    item.ScriptIncludedInProject = false;
                }
            }
        }
    }
}
