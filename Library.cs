﻿using AGS.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace AGS.Plugin.Libraries
{
    public class Library
    {
        public const string HEADER_FILE_EXT = ".ash";
        public const string SCRIPT_FILE_EXT = ".asc";
        private ScriptAndHeader _script;

        private static string FindFirstAvailableFileName(string prefix)
        {
            int attempt = 0;
            string newFileName;
            do
            {
                newFileName = prefix + ((attempt > 0) ? attempt.ToString() : string.Empty);
                attempt++;
            }
            while ((File.Exists(newFileName + SCRIPT_FILE_EXT)) ||
                   (File.Exists(newFileName + HEADER_FILE_EXT)));

            return newFileName;
        }

        internal Library(XmlNode node)
        {
            IAGSEditor editor = LibrariesPlugin.EditorInstance;
            if ((editor == null) || (editor.CurrentGame == null))
            {
                throw new InvalidOperationException("Cannot create Library object when no game is loaded");
            }
            if ((node == null) || (node.Name != "Library"))
            {
                throw new AGS.Types.InvalidDataException("Library node incorrect" + (node == null ? " (node is null)" : ": " + node.Name));
            }
            string author = SerializeUtils.GetElementString(node, "Author");
            string description = SerializeUtils.GetElementString(node, "Description");
            string fileName = SerializeUtils.GetElementString(node, "FileName");
            string name = SerializeUtils.GetElementString(node, "Name");
            string uniqueKeyString = SerializeUtils.GetElementString(node, "UniqueKey");
            string version = SerializeUtils.GetElementString(node, "Version");
            int uniqueKey;
            if (!int.TryParse(uniqueKeyString, out uniqueKey))
            {
                throw new AGS.Types.InvalidDataException("Library unique key incorrect");
            }
            _script = null;
            foreach (ScriptAndHeader scriptAndHeader in editor.CurrentGame.ScriptsAndHeaders)
            {
                if (scriptAndHeader.Header.UniqueKey == uniqueKey)
                {
                    _script = scriptAndHeader;
                    _script.Header.Author = author;
                    _script.Header.Description = description;
                    _script.Header.FileName = fileName + HEADER_FILE_EXT;
                    _script.Header.Name = name;
                    _script.Header.Version = version;
                    _script.Script.Author = author;
                    _script.Script.Description = description;
                    _script.Script.FileName = fileName + SCRIPT_FILE_EXT;
                    _script.Script.Name = name;
                    _script.Script.Version = version;
                    break;
                }
            }
            if (_script == null)
            {
                string headerFile = Libraries.GetFilePath(uniqueKey.ToString() + HEADER_FILE_EXT);
                string scriptFile = Libraries.GetFilePath(uniqueKey.ToString() + SCRIPT_FILE_EXT);
                if ((!File.Exists(headerFile)) || (!File.Exists(scriptFile)))
                {
                    throw new FileNotFoundException("Library files do not exist: " + (string.IsNullOrEmpty(name) ? fileName : name));
                }
                string newFileName = FindFirstAvailableFileName(fileName);
                Script header = new Script(newFileName + HEADER_FILE_EXT, File.ReadAllText(headerFile), name, description, author, version, uniqueKey, true);
                Script script = new Script(newFileName + SCRIPT_FILE_EXT, File.ReadAllText(scriptFile), name, description, author, version, uniqueKey, false);
                _script = new ScriptAndHeader(header, script);
            }
        }

        internal Library(ScriptAndHeader script)
        {
            if (script == null) throw new ArgumentNullException("Invalid script");
            _script = script;
        }

        public void CreateHardLink(HardLinkDirection direction)
        {
            if (direction == HardLinkDirection.GlobalCacheToLocal)
            {
                Utilities.CreateHardLink(Libraries.GetProjectFilePath(Header.FileName), Libraries.GetFilePath(CachedHeaderFileName), true);
                Utilities.CreateHardLink(Libraries.GetProjectFilePath(Script.FileName), Libraries.GetFilePath(CachedScriptFileName), true);
            }
            else
            {
                Utilities.CreateHardLink(Libraries.GetFilePath(CachedHeaderFileName), Libraries.GetProjectFilePath(Header.FileName), true);
                Utilities.CreateHardLink(Libraries.GetFilePath(CachedScriptFileName), Libraries.GetProjectFilePath(Script.FileName), true);
            }
        }

        internal ScriptAndHeader SetUniqueKey(int uniqueKey)
        {
            return SetUniqueKey(uniqueKey, Header.Text, Script.Text);
        }

        internal ScriptAndHeader SetUniqueKey(int uniqueKey, string headerText, string scriptText)
        {
            Script header = new Script(Header.FileName, headerText, Name, Description, Author, Version, uniqueKey, true);
            Script script = new Script(Script.FileName, scriptText, Name, Description, Author, Version, uniqueKey, false);
            _script = new ScriptAndHeader(header, script);
            return _script;
        }

        /// <summary>
        /// Generates a new unique key to differentiate a local script from a library,
        /// and breaks the hard link.
        /// </summary>
        public ScriptAndHeader MakeUniqueCopy()
        {
            SetUniqueKey(new Random().Next(Int32.MaxValue));
            ScriptIncludedInProject = true;
            LibrariesPlugin.LibrariesPane.SetItemChecked(this, false);
            return _script;
        }

        public string Author
        {
            get
            {
                return Header.Author;
            }

            set
            {
                Header.Author = value;
                Script.Author = value;
            }
        }

        public string Description
        {
            get
            {
                return Header.Description;
            }

            set
            {
                Header.Description = value;
                Script.Description = value;
            }
        }

        public bool FilesExist
        {
            get
            {
                return ((File.Exists(Libraries.GetFilePath(CachedHeaderFileName))) && (File.Exists(Libraries.GetFilePath(CachedScriptFileName))));
            }
        }

        public string FileName
        {
            get
            {
                return Path.GetFileNameWithoutExtension(Header.FileName);
            }

            set
            {
                Header.FileName = (value + HEADER_FILE_EXT);
                Script.FileName = (value + SCRIPT_FILE_EXT);
            }
        }

        public bool HasLocalFiles
        {
            get
            {
                return ((File.Exists(Libraries.GetProjectFilePath(Header.FileName))) &&
                    (File.Exists(Libraries.GetProjectFilePath(Script.FileName))));
            }
        }

        public Script Header
        {
            get
            {
                return _script.Header;
            }
        }

        public string CachedHeaderFileName
        {
            get
            {
                return UniqueKey.ToString() + HEADER_FILE_EXT;
            }
        }

        public bool Included
        {
            get
            {
                return LibrariesPlugin.LibrariesPane.GetItemChecked(this);
            }

            set
            {
                LibrariesPlugin.LibrariesPane.SetItemChecked(this, value);
            }
        }

        public string Name
        {
            get
            {
                return Header.Name;
            }

            set
            {
                Header.Name = value;
                Script.Name = value;
            }
        }

        public Script Script
        {
            get
            {
                return _script.Script;
            }
        }

        public ScriptAndHeader ScriptAndHeader
        {
            get
            {
                return _script;
            }
        }

        public string CachedScriptFileName
        {
            get
            {
                return UniqueKey.ToString() + SCRIPT_FILE_EXT;
            }
        }

        public bool ScriptIncludedInProject
        {
            get
            {
                int idx;
                ScriptAndHeader script = LibrariesPlugin.EditorInstance.CurrentGame.ScriptsAndHeaders.GetScriptAndHeaderByFilename(Header.FileName, out idx);
                return ((script != null) && (script.Header.UniqueKey == UniqueKey) && (HasLocalFiles));
            }

            set
            {
                Game game = (Game)LibrariesPlugin.EditorInstance.CurrentGame;
                int idx;
                ScriptAndHeader oldScriptAndHeader = game.ScriptsAndHeaders.GetScriptAndHeaderByFilename(Script.FileName, out idx);
                bool wasGameModified = game.FilesAddedOrRemoved;
                File.Delete(Libraries.GetProjectFilePath(Header.FileName)); // does not throw
                File.Delete(Libraries.GetProjectFilePath(Script.FileName));
                if (oldScriptAndHeader != null)
                {
                    game.ScriptsAndHeaders.Remove(oldScriptAndHeader);
                }
                if (value)
                {
                    if (oldScriptAndHeader != null)
                    {
                        game.ScriptsAndHeaders.AddAt(_script, idx);
                        game.FilesAddedOrRemoved = wasGameModified;
                    }
                    else
                    {
                        game.ScriptsAndHeaders.AddAt(_script, 0);
                        LibrariesPlugin.ScriptsComponent.RePopulateTreeView();
                        game.FilesAddedOrRemoved = wasGameModified;
                    }
                    Header.Modified = true;
                    Script.Modified = true;
                    Header.SaveToDisk();
                    Script.SaveToDisk();
                }
                else LibrariesPlugin.ScriptsComponent.RePopulateTreeView();
            }
        }

        public int UniqueKey
        {
            get
            {
                return Header.UniqueKey;
            }
        }

        public string Version
        {
            get
            {
                return Header.Version;
            }

            set
            {
                Header.Version = value;
                Script.Version = value;
            }
        }

        public override string ToString()
        {
            return (string.IsNullOrEmpty(Name) ? Path.GetFileNameWithoutExtension(Header.FileName) : Name);
        }
    }
}
